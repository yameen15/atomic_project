<?php

include_once ('../../../vendor/autoload.php');

use App\OrgName\OrgID\Mobile\Mobile;
use App\OrgName\OrgID\Message\Message;
use App\OrgName\OrgID\Utility\Utility;
$mobile= new Mobile();
    $result=$mobile->setData($_POST)->is_unique();
    $singleData= $mobile->setData($_POST)->show();

if($result){
    Message::message("The title you want to update is already been taken");
    Utility::redirect('edit.php?id='.$singleData->id);
}
else {

    if (isset($_POST['title']) && !empty($_POST['title'])) {
        if (preg_match("/([A-Za-z0-9_-])/", $_POST['title'])) {
            $mobile = new Mobile();
            $mobile->setData($_POST)->update();
        }
        else{
            Message::message("Please insert valid data");
            Utility::redirect('edit.php?id='.$singleData->id);

        }
    } else {
        Message::message("Please insert some data");
        Utility::redirect('edit.php?id='.$singleData->id);
    }
}