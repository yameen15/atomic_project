<?php

session_start();

include_once ('../../../vendor/autoload.php');

use App\OrgName\OrgID\Mobile\Mobile;
use App\OrgName\OrgID\Message\Message;
use App\OrgName\OrgID\Utility\Utility;

$mobile = new Mobile();

//Pagination
if(array_key_exists('itemPerPage',$_SESSION)){
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];

    }
}else{
    $_SESSION['itemPerPage']=5;
}
$itemPerPage= $_SESSION['itemPerPage'];
$totalItem= $mobile->count();
$totalPage= ceil($totalItem/$itemPerPage);
$pagination="";
if(array_key_exists('pageNumber',$_GET)) {
    $pageNumber = $_GET['pageNumber'];
}
else{
    $pageNumber=$_GET['pageNumber']=1;
}
$pagination="";
for($i=1;$i<=$totalPage;$i++){

    $pagination.="<a href='index.php?pageNumber=".$i."'>".$i."</a>";
}
$pageStartFrom=$itemPerPage*($pageNumber-1);



$allTitle=$mobile->setData($_POST)->paginate($pageStartFrom,$itemPerPage);


?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>All mobile info</title>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<a href="create.php">Create</a>|
<a href="trashed.php">View all trashed items</a>
<br>
<br>
<form action="index.php" method="post">
    <button type="submit" name="lef" value="lastenterfirst">View as Last Enter First</button>
</form>
<br>
<form action="index.php" method="post">
    <button type="submit" name="lef" value="">View as Alphabetic Order</button>
</form>
<div id="message">
    <?php
    if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
        echo Message::message();
    } ?>

</div>
<form action="index.php">
    <div>
        <label>Select Number of item you want to see (select one):</label>
        <select name="itemPerPage">
            <option>5</option>
            <option>10</option>
            <option>15</option>
            <option>20</option>
            <option>25</option>
        </select>
        <br>
        <button type="submit">Go!</button>

    </div>
</form>

<table>
    <thead>
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Mobile Title</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php $sl=0;
    foreach ($allTitle as $data){
    $sl++;
    ?>
    <tr>
        <td><?php echo $sl+$pageStartFrom ?></td>
        <td><?php echo $data->id ?></td>
        <td><?php echo $data->title ?></td>
        <td><a href="view.php?id=<?php echo $data->id ?>">View</a>|
            <a href="edit.php?id=<?php echo $data->id ?>">Edit</a>|
            <a href="delete.php?id=<?php echo $data->id?> " Onclick="ConfirmDelete()">Delete</a>|
            <a href="trash.php?id=<?php echo $data->id?>">Trash</a>
        </td>
    </tr>
    <?php } ?>
    </tbody>
</table>
<?php echo $pagination ?>
</body>
</html>
<script>


    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>


