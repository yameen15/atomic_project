<?php
include_once('../../../vendor/autoload.php');

use App\OrgName\OrgID\Mobile\Mobile;
use  App\OrgName\OrgID\Message\Message;
use App\OrgName\OrgID\Utility\Utility;

$obj = new Mobile();
$obj->setData($_GET)->recover();
