<?php

include_once ('../../../vendor/autoload.php');

use App\OrgName\OrgID\Mobile\Mobile;
use App\OrgName\OrgID\Message\Message;
use App\OrgName\OrgID\Utility\Utility;

$mobile = new Mobile();
$singleTitle= $mobile->setData($_GET)->show();

?>


<!DOCTYPE html>
<html>
<head>
    <title>Single Data</title>
</head>
<body>
<button onclick="goBack()">Go Back</button>
<br>
<br>
<table border="1">
    <tr>
        <th>ID:</th>
        <th><?php echo $singleTitle->id?></th>
    </tr>
    <tr>
        <td>Title:</td>
        <td><?php echo $singleTitle->title?></td>
    </tr>
</table>
</body>
</html>
<script>
    function goBack() {
        window.history.back();
    }
</script>



