<?php

include_once ('../../../vendor/autoload.php');

use App\OrgName\OrgID\Mobile\Mobile;
use App\OrgName\OrgID\Message\Message;
use App\OrgName\OrgID\Utility\Utility;
$mobile= new Mobile();
$result=$mobile->setData($_POST)->is_unique();

if($result){
    Message::message("The title you insert is already been taken");
    Utility::redirect('create.php');
}
else {

    if (isset($_POST['title']) && !empty($_POST['title'])) {
        if (preg_match("/([A-Za-z0-9._-])/", $_POST['title'])) {
            $mobile = new Mobile();
            $mobile->setData($_POST)->store();
        }
        else{
            Message::message("Please insert valid data");
            Utility::redirect("create.php");

        }
    } else {
        Message::message("Please insert some data");
        Utility::redirect("create.php");
    }
}