<?php
include_once('../../../vendor/autoload.php');

use App\OrgName\OrgID\Mobile\Mobile;
use  App\OrgName\OrgID\Utility\Utility;

use App\OrgName\OrgID\Message\Message;

if($_SERVER['REQUEST_METHOD']=='POST'){
    $mobiles = new Mobile();
    $mobile = $mobiles->deleteOlderData();

}else {

    $mobiles = new Mobile();
    $mobile = $mobiles->setData($_GET)->trashed();
}


?>

<!DOCTYPE html>
<html>
<head>
    <title>Trashed</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        #message{
            background-color:green;
        }

    </style>
</head>
<body>
<h1>Mobile Title</h1>

<div id="message">
    <?php echo Message::message(); ?>
</div>
<form action="trashed.php" method="post">
    <button type="submit">Delete 10 days older data</button>
</form>
<table border="1">
    <thead>
    <tr>
        <th>Sl.</th>
        <th>ID</th>
        <th>Title&dArr;</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if(count($mobile) > 0){

        $slno =1;
        foreach($mobile as $data){
            ?>
            <tr>
                <td><?php echo $slno;?></td>
                <td><?php echo $data->id;?></td>
                <td><?php echo $data->title;?></td>
                <td>
                    <a href="recover.php?id=<?php echo $data->id;?>">Recover</a>
                    | <a href="delete.php?id=<?php echo $data->id;?>" class="delete">Delete</a>

                </td>
            </tr>
            <?php
            $slno++;
        }

    }else{
        ?>
        <tr>
            <td colspan="6">No record is available.</td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
</form>

<div><span> prev  1 | 2 | 3 next </span></div>

<nav>
    <li><a href="index.php">Go to list</a></li>
</nav>
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
<script>


    $(document).ready(function() {

        $('.delete').bind('click', function (e) {
            var deleteItem = confirm("Are you sure you want to delete?");
            if (!deleteItem) {
                //return false;
                e.preventDefault();
            }
        });
        $('#message').hide(5000);


    }




</script>
</body>
</html>
