<?php

session_start();

include_once ('../../../vendor/autoload.php');
use App\OrgName\OrgID\Message\Message;
?>

<!DOCTYPE html>
<html>
    <body>
        <div id="message">
            <?php
            if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
                echo Message::message();
            }?>
        </div>

        <form action="store.php" method="POST">
            <fieldset>
                <legend>Mobile information:</legend>
                <level>Title</level>
                    <input type="text" name="title" placeholder="Enter your mobile title">
                        <br>
                        <br>
                            <button type="submit" style="background-color:transparent; border-color:transparent;">
                                <img src="../../../Resources/Button/submit.jpg" height="35"/>
                            </button>

                            <button type="submit" name="saveAdd" value="saveAdd">
                                Save & Add
                            </button>

                            <button type="reset" style="background-color:transparent; border-color:transparent;">
                                <img src="../../../Resources/Button/reset.jpg" height="35"/>
                            </button>
                         <br>
            </fieldset>
        </form>

    </body>
</html>
